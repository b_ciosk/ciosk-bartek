$(document).ready(function() {
    var currentSymbol = 'O',
        usedIndexes =[];
    $('.col-md-4').click(function() {
        //console.log("klinknięto w " + $(this).data('pos'));
        id=parseInt($(this).data('pos'))
        if (usedIndexes.indexOf(id) == -1 ) {
            $(this).html(currentSymbol);
            usedIndexes.push($(this).data('pos'));
            console.log(checkForWin());
            checkGameStatus();
            currentSymbol = (currentSymbol=='O') ? 'X' : 'O';
           
        };
    });
    
    function checkGameStatus() {
        var isWinner = checkForWin();
        if (isWinner != false) {
            //$('.row').css('display','none');
            $('.tryAgain').html("WYGRAŁ gracz '" + currentSymbol + "' - Zagraj Ponownie");
            $('h1').css('display','none');
            $('.tryAgain').css('display','inline');
            for(i=0;i<isWinner.length;i++) {
                $('[data-pos =' + isWinner[i] + ']').css('background-color','lightblue');
            }
        
        }
        else if (usedIndexes.length==9) {
            $('.tryAgain').html('REMIS - Zagraj Ponownie');
            $('h1').css('display','none');
            $('.tryAgain').css('display','inline'); 
        }
        
        
    };
    
    $('.tryAgain').click(function() {
        window.location.reload();
        
    });
    
    function checkForWin() {
        var winPosibilities = [
            [1,2,3],
            [4,5,6],
            [7,8,9],
            [1,4,7],
            [2,5,8],
            [3,6,9],
            [1,5,9],
            [3,5,7]
        ];
        
        for(var i =0; i<winPosibilities.length;i++) {
            var currSearch = winPosibilities[i],
                elem = [],
                cs=currentSymbol;
            elem.push($('[data-pos =' + currSearch[0] + ']').html());
            elem.push($('[data-pos =' + currSearch[1] + ']').html());
            elem.push($('[data-pos =' + currSearch[2] + ']').html());
            if (elem[0] == cs && elem[1] == cs && elem[2] == cs) {
                return currSearch;
                }
        }
            return false;
    };
});