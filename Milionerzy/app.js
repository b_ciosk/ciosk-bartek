$(document).ready(function() {
    
    var questions = [
        {"question" : "Ile czasu potrzeba, aby przejść z punktu A do punktu B?",
        'answers': ['5 godzin','2 lata','Całe życie','Życia nie starczy'],
         'correctAnswer' : '2 lata',
         'tip': 'Więcej niż kilka godzin.'
        },
        {"question" : "2+2*2 = ?",
        'answers': ['8','4','6','222'],
         'correctAnswer' : '6',
         'tip': 'Nie wiem Andrzej, nie wiem.'
        },
        {"question" : "Które z tych zwierząt jest największe",
        'answers': ['kaczka','koń','pies','kot'],
         'correctAnswer' : 'koń',
         'tip': 'Prawdopodobnie koń'
        },
        {"question" : "Więcej niż jedno zwierzę to?",
        'answers': ['jeżozwierze','owca','stado','lama'],
         'correctAnswer' : 'stado',
         'tip': 'Chyba owca albo lama'
        }
 
        ],
        usedIndexes = [],
        currentQuestion = getRandomQuestion(questions,usedIndexes);
        
    
    
    
    function fillData(obj) {
        getInputsEndisabled();
        obj['answers'] = shuffle(obj['answers']);
        $('.pytanie').html(obj['question']);
        $('.answer').each(function(i,e) {
            e.innerHTML = (obj['answers'][i]);
            
        });
    }
    
    function getRandomQuestion(questions,usedIndexes) {
        var index = getRandomIndex(questions,usedIndexes);
        if (index==undefined) {
            winGame();
        }
        
        return questions[index];
        
    }
    function getHelpFromFriend(currentQuestion) {
        alert(currentQuestion['tip']);
    }
    $('.friendHelp').click(function() {
        getHelpFromFriend(currentQuestion);
        $('.friendHelp').css('opacity','0');
        $('.friendHelp').attr('disabled','disabled');
        return currentQuestion['tip'];
    });
    function getFiftyFiftyHelp(currentQuestion) {
        var tempArr = [];
        $('.answer').each(function(i,e) {
            if (e.innerHTML != currentQuestion['correctAnswer']) {
                tempArr.push(e);
            }
        });
        tempArr.pop().disabled='disabled';
        tempArr.pop().disabled='disabled';
/*        if ($('.answer').attr.disabled =='disabled') {
            $('.answer').css('opacity','0');
        };*/
        $('.fiftyFiftyHelp').attr('disabled','disabled');
        $('.fiftyFiftyHelp').css('opacity','0');
                
    };
    
    function shuffle(arr){
        var rand,temp;
        for(var i = arr.length; i > 0; i--){
            rand = (Math.random() * i) | 0;
            temp = arr[i-1];
            arr[i -1] = arr[rand];
            arr[rand] = temp;
	
	   }
	       return arr;
    };
        
        
        
        
    
    
    
/*    function getAudienceHelp(currentQuestion) {
        var tempArray=[]
        var random;
        for (i=0;i<4;i++){
            random=(Math.random()*50)|0;
            tempArray[i] =random;
        };
        var suma=0;
        var usedIndexes2 = []
        for(i=0;i<4;i++){
            suma+=tempArray[i];
        };
        var tempArrayInPercent=[]
	       for (i=0;i<4;i++) {
		   tempArrayInPercent.push((tempArray[i]/suma*100)|0);
           };
        var max=tempArrayInPercent[0];
        for(i=0;i<4;i++){
            if (tempArrayInPercent[i]>max) {
		      max=tempArrayInPercent[i];
	       };
        };
        var lastArray = [
            {'a': 0},
            {'b': 0},
            {'c': 0},
            {'d': 0},
        ];
        nowa=[];
        console.log(tempArrayInPercent);
        for (var i in tempArrayInPercent) {
            if (tempArrayInPercent[i]!=max) {
                nowa.push(tempArrayInPercent[i]);
            }
        }
        $('.answer').each(function() {
            if ($(this).html()==currentQuestion['correctAnswer']) {
                lastArray[$(this).val()] = max;
                
            }
            else {
                lastArray[$(this).val()] = nowa[getRandomIndex(nowa,usedIndexes2)];
            }
        });
        $('.audienceHelp').attr('disabled','disabled');
        $('.audienceHelp').css('opacity','0');
        alert('Wyniki głosowanie publiczności \n' + 'Odpowiedź A: '+lastArray['a'] + '%\n'+ 'Odpowiedź B: '+lastArray['b'] + '%\n'+ 'Odpowiedź C: '+lastArray['c'] + '%\n'+ 'Odpowiedź D: '+lastArray['d'] + '%');
    };*/
    
    function getHelpFromAudience(currentQuestion) {
            var tempValues = [],
                sum=0,
                tempPercent=[],
                str='';
            for (var i =0; i<4;i++) {
                tempValues[i]=(Math.random()*50) |0;
                sum+=tempValues[i];
            }
            for (var i=0; i<4;i++) {
                tempPercent[i]= (tempValues[i]/sum*100) |0;
            }
        tempPercent = tempPercent.sort(function(a,b) {return a-b});
        $('.answer').each(function(i,e) {
            //console.log(e.dataset.letter, (e.innerHTML==currentQuestion['correctAnswer']));
            if (e.innerHTML==currentQuestion['correctAnswer']) {
                str+= e.dataset.letter +': '+tempPercent.pop() + '% \n';
            }
            else {
                str+= e.dataset.letter +': '+tempPercent.shift() + '% \n';
            }
        })
        alert(str);
        $('.audienceHelp').attr('disabled','disabled');
        $('.audienceHelp').css('opacity','0');
    }
    
    
    $('.fiftyFiftyHelp').click(function() {
        getFiftyFiftyHelp(currentQuestion);
        
    });
    

    
    $('.audienceHelp').click(function() {
        /*getAudienceHelp(currentQuestion)*/;
        getHelpFromAudience(currentQuestion);
        
    });
    
    function getInputsEndisabled() {
        $('.answer').each(function() {
            $(this).removeAttr('disabled');
        })
    }
    
    
    function getRandomIndex(questions,usedIndexes) {
        var elem =  questions.length,
            tempArr = [],
            random;
        for (var i =0;i<elem;i++) {
            if (usedIndexes.indexOf(i) == -1) tempArr.push(i);
        }
        random = (Math.random()*tempArr.length) |0;
        usedIndexes.push(tempArr[random]);
        return tempArr[random];
    }

    
    
    function endGame(){
            $('.answer').css('display','none');
            $('.pytanie').html("PRZEGRAŁEŚ").css('color','white','background-color','red').css('background-color','red');
            $('.tryAgain').css('display','inline');
    }
        

    function winGame() {
        $('.answer').css('display','none');
        $('.pytanie').html("WYGRAŁEŚ").css('color','white').css('background-color','#0fc90e');
        $('.tryAgain').css('display','inline');
    }
    
    $('.answer').click(function () {
        
        if ($(this).html()==currentQuestion['correctAnswer']) {
            console.log('Brawo');
            currentQuestion = getRandomQuestion(questions,usedIndexes);
            fillData(currentQuestion);
        }
        else {
            endGame();
        }
    });    
    
    
    fillData(currentQuestion);
    $('.tryAgain').click(function(){
        window.location.reload();
    });
    
});