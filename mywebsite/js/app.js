$(document).ready(function() {
   
    $('.changeView').click(function() {
        if ($('.changeView').hasClass('active')) {
            $('.container').css('background-color','black');
            $('.container').css('color','white');
            $('.changeView').html('Włącz tryb dzienny')
            $('.changeView').removeClass('active');
        }
        else {
            $('.container').css('background-color','white');
            $('.container').css('color','black');
            $('.changeView').html('Włącz tryb nocny')
            $('.changeView').addClass('active') 
        }
    });
    
    $("#owl-demo").owlCarousel({
 
      autoPlay: 3000, //Set AutoPlay to 3 seconds
 
      items : 4,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3],
    });
    
    $('#contactForm').validate({
        
        rules : {
            "name" : {required : true, minlength: 3},
            "email" : {required : true, email : true},
            "content" : { required : true, minlength: 10}
        },
        
        messages : {
            "name" : "Przedstaw się.",
            "email" : "Wprowadź poprawny email",
            "content" : "Przydałaby się jakaś treść"
        }
        
    });
    
    
    
    
    
    
    
    
    
    
    
    
    
});